require 'rails_helper'

RSpec.describe "Posts", type: :request do
  describe "GET /posts" do
    before do
      create_list :post, 2
    end

    let(:posts) { Post.all.order(id: :desc) }
    let(:posts_to_json) { Oj.dump posts, mode: :compat }
    let(:user_keys) { json.first['user'].keys }

    it "returns all posts" do
      get posts_path

      expect(response).to be_success
      expect(response.content_type).to eq 'application/json'
      expect(response.body).to include posts_to_json
      expect(user_keys).to_not include 'id'
      expect(user_keys).to_not include 'created_at'
      expect(user_keys).to_not include 'updated_at'
    end

    context 'pagination' do
      let(:first_post) { Post.first }
      let(:second_post) { Post.second }
      let(:first_post_to_json) { Oj.dump first_post, mode: :compat }
      let(:second_post_to_json) { Oj.dump second_post, mode: :compat }

      context 'when pass after_id' do
        it 'returns only the posts after that id' do
          get posts_path, after_id: first_post.id

          expect(response).to be_success
          expect(response.body).to_not include first_post_to_json
          expect(response.body).to include second_post_to_json
          expect(json.count).to eq 1
        end
      end

      context 'when pass before_id' do
        it 'returns only the posts before that id' do
          get posts_path, before_id: second_post.id

          expect(response).to be_success
          expect(response.body).to_not include second_post_to_json
          expect(response.body).to include first_post_to_json
          expect(json.count).to eq 1
        end
      end
    end
  end
end
