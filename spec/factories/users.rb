FactoryGirl.define do
  factory :user do
    full_name "Lucas DAvila"
    latitude 51.5034070
    longitude -0.1275920
    avatar 'https://foocdn.com/avatar.jpg'
  end

end
