class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :full_name
      t.decimal :latitude, precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6
      t.string :avatar

      t.timestamps null: false
    end

    add_index :users, [ :latitude, :longitude ]
  end
end
