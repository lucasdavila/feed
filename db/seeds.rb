# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = []

for i in 1..10 do
  user = User.create!(
    full_name: "User #{i}",
    latitude: 51.5034070,
    longitude: -0.1275920,
    avatar: 'https://foocdn.com/avatar.jpg'
  )

  puts "created user #{user.inspect}"

  users << user
end

for i in 1..100000 do
  post = Post.create!(
    user: users.sample,
    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
      nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
      reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
      pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
      qui officia deserunt mollit anim id est laborum."
  )

  puts "created post #{post.inspect}"
end
