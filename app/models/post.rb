class Post < ActiveRecord::Base
  validates :body, :user_id, presence: true

  belongs_to :user

  def as_json(options = {})
    super(
      include: {
        user: { only: [ :full_name, :latitude, :longitude, :avatar ] }
      }
    )
  end
end
