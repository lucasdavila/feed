class User < ActiveRecord::Base
  validates :full_name, presence: true

  has_many :posts, dependent: :destroy
end
