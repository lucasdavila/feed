class PostsController < ApplicationController
  def index
    @posts = Post.all

    if params[:after_id].present?
      @posts = @posts.where 'id > ?', params[:after_id]
    end

    if params[:before_id].present?
      @posts = @posts.where 'id < ?', params[:before_id]
    end

    @posts = @posts.order id: :desc
    @posts = @posts.limit default_limit
    @posts = @posts.includes(:user)

    render json: Oj.dump(@posts, mode: :compat)
  end

  protected

  def default_limit
    # using a high limit just for test purposes
    # in a real api it would use a small value
    1000
  end
end
